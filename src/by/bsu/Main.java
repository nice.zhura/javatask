package by.bsu;

import by.bsu.transformator.Transform;
import by.bsu.transformator.Validator;

public class Main {
    public static void main(String[] args) {
        // Initialize input string
        String inputString = "( 10 * ( 3 + ( 4 / 4 ) ) )";

        // Print input string
        System.out.println("Infix notation of mathematical expression:");
        System.out.println(inputString);
        System.out.println();

        try {
            // Validate input string
            Validator.inputStringValidator(inputString);

            // Transform expression into prefix notation
            String prefixString = Transform.infixToPrefix(inputString);

            // Print string in prefix notation
            System.out.println("Prefix notation of mathematical expression:");
            System.out.println(prefixString);
            System.out.println();

            // Validate input string in prefix notation
            Validator.prefixStringValidator(prefixString);

            // Compute expression in prefix notation
            double result = Transform.computePrefixNotation(prefixString);

            // Print computation of expression inb prefix notation
            System.out.println("Computation of mathematical expression:");
            System.out.println(result);
            System.out.println();
        } catch (Exception ex) {
            // Catch exceptions into err output stream
            System.err.println("Exception!");
            System.err.println(ex.getMessage());
        }
    }
}
