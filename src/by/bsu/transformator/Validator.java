package by.bsu.transformator;

public class Validator {
    // Validating input before parsing into prefix notation
    public static void inputStringValidator(String inputString) throws Exception {
        // Check if number of opening parenthesis is equal to the number of closing ones
        int numberOfOpeningParenthesis = 0;
        int numberOfClosingParenthesis = 0;

        // Count number of opening and closing parenthesis
        for (int i = 0; i < inputString.length(); ++i) {
            char item = inputString.charAt(i);
            if (item == '(') {
                numberOfOpeningParenthesis++;
            } else if (item == ')') {
                numberOfClosingParenthesis++;
            }
        }

        // Throw exception if number of opening parenthesis is not equal to the number of closing ones
        if (numberOfOpeningParenthesis - numberOfClosingParenthesis != 0) {
            throw new Exception("Different number of opening and closing parenthesis!");
        }

        // Check if the string matches the template
        for (int i = 0; i < inputString.length() - 1; i = i + 1) {
            char item = inputString.charAt(i);
            char nextItem = inputString.charAt(i + 1);

            boolean isItemOperand = Tool.isSymbol(item) || Tool.isDigit(item);
            boolean isNextItemOperand = Tool.isSymbol(nextItem) || Tool.isDigit(nextItem);

            if (isItemOperand && (isNextItemOperand || nextItem == ' ')) {
                continue;
            }

            boolean isItemOperator = Tool.isOperator(item) || Tool.isParenthesis(item);
            boolean isNextItemOperator = Tool.isOperator(nextItem) || Tool.isParenthesis(nextItem);

            if (isItemOperator && nextItem == ' ') {
                continue;
            }

            if (item == ' ' && (isNextItemOperand || isNextItemOperator)) {
                continue;
            }

            throw new Exception("The string doesn't match the template!");
        }
    }

    // Validating input string in prefix notation before computing it
    public static void prefixStringValidator(String prefixString) throws Exception {
        for (int i = 0; i < prefixString.length(); ++i) {
            // Get character of the string
            char item = prefixString.charAt(i);

            // Check if character is operator or digit
            boolean isValidCharacter = Tool.isParenthesis(item) || Tool.isOperator(item) || Tool.isDigit(item) || item == ' ';

            if (isValidCharacter) {
                continue;
            }

            // If input string in prefix notation isn't numeric expression throw exception
            throw new Exception("Input string in prefix notation isn't numeric expression!");
        }
    }
}
