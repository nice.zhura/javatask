package by.bsu.transformator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Stack;

public class Tool {
    // Check if char item is operator
    public static boolean isOperator(char item) {
        return (item == '*' || item == '/' || item == '+' || item == '-');
    }

    // Check if char item is symbol
    public static boolean isSymbol(char item) {
        return (item >= 'a' && item <= 'z') || (item >= 'A' && item <= 'Z');
    }

    // Check if char item is digit
    public static boolean isDigit(char item) {
        return (item >= '0' && item <= '9');
    }

    // Check if char item is parenthesis
    public static boolean isParenthesis(char item) {
        return (item == '(' || item == ')');
    }

    // Get priority of operator in char variable
    private static int getPriority(char item) throws Exception {
        if (isOperator(item)) {
            if (item == '*' || item == '/') {
                return 2;
            } else if (item == '+' || item == '-') {
                return 1;
            }
        }
        throw new Exception("Wrong operator!");
    }

    // Check if first char has greater or equal priority to the second one
    public static boolean hasGraterPriorityThan(char firstItem, char lastItem) throws Exception {
        return getPriority(firstItem) >= getPriority(lastItem);
    }

    // Compute expression according to operator
    public static double computeExpression(double firstNumber, double lastNumber, char operation) throws Exception {
        // Check if operator is valid
        if (!isOperator(operation)) {
            throw new Exception("Wrong operator in computing expression");
        }

        // Compute expresion
        double result = 0.0;
        if (operation == '+') {
            result = firstNumber + lastNumber;
        } else if (operation == '-') {
            result = firstNumber - lastNumber;
        } else if (operation == '*') {
            result = firstNumber * lastNumber;
        } else if (operation == '/') {
            if (lastNumber == 0.0) {
                throw new Exception("Division by zero in computing expression!");
            } else {
                result = firstNumber / lastNumber;
            }
        }

        // Return result of computing
        return result;
    }

    // Get information from xml table
    public static Stack<String> getRowsFromTable(String path) throws Exception {
        // Create stack of strings for elements of table
        Stack<String> table = new Stack<>();

        // Read files from InfixToPrefixData
        InputStream inputStream = new FileInputStream(path);
        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

        // Get first sheet
        Sheet sheet = workbook.getSheetAt(0);

        // Get iterator of a row of a sheet
        Iterator<Row> rowIterator = sheet.iterator();

        // Skip first row
        rowIterator.next();

        // Go through the whole sheet by rows
        while (rowIterator.hasNext()) {
            // Get row iterator
            Row row = rowIterator.next();

            // Get the cell iterator
            Iterator<Cell> cellIterator = row.iterator();

            // Get string in infix notation
            Cell cell = cellIterator.next();

            // Get first cell
            String firstCell = cell.getStringCellValue();

            // If we read empty cell
            if (firstCell.isEmpty()) {
                break;
            }

            // Push into stack
            table.push(firstCell);

            // Get string in prefix notation
            cell = cellIterator.next();

            // Get second cell
            String secondCell = cell.getStringCellValue();

            // Push into stack
            table.push(secondCell);
        }

        // Return information from table according to the path
        return table;
    }
}
