package by.bsu.transformator;

import java.util.Stack;

public class Transform {
    // Transform non empty string into stack of characters
    private static Stack<Character> stringIntoStack(String inputString) throws Exception {
        // If input string is empty throw exception
        if (inputString.trim().isEmpty()) {
            throw new Exception("Input string is empty!");
        }

        // Initialize output stack
        Stack<Character> outputStack = new Stack<>();

        // Parse input string
        for (int i = 0; i < inputString.length(); ++i) {
            // Get the element of the string
            char item = inputString.charAt(i);

            // Push data into input stack
            outputStack.push(item);
        }

        // Return output stack
        return outputStack;
    }

    // Transform non empty stack into string
    private static String stackIntoString(Stack<Character> inputStack) throws Exception {
        // If input stack is empty throw exception
        if (inputStack.empty()) {
            throw new Exception("Input stack is empty!");
        }

        // Transform prefix string into string builder
        StringBuilder outputString = new StringBuilder();
        while (!inputStack.empty()) {
            outputString.append(inputStack.pop());
        }

        // Return string builder in string format
        return outputString.toString();
    }

    // Transform infix notation into prefix notation
    public static String infixToPrefix(String inputString) throws Exception {
        // Initialize stacks for algorithm's transformation of data

        // Transform characters from inputString into stack infixString
        Stack<Character> infixString = stringIntoStack(inputString);

        // Stack for operators
        Stack<Character> operators = new Stack<>();

        // Stack for prefix notation
        Stack<Character> prefixString = new Stack<>();

        // Algorithm of string transformation into prefix notation

        // While infixString stack isn't empty
        while (!infixString.empty()) {
            // Get the element from the infix string
            char item = infixString.pop();

            // If it is closing parenthesis, push it on stack
            if (item == ')') {
                operators.push(item);

                // And skip empty element
                infixString.pop();
            }

            // If it is an operator, then
            else if (Tool.isOperator(item)) {
                // Create variable to check if item is processed
                boolean isPushed = false;

                while (!isPushed) {
                    // If stack is empty, push operator on stack
                    if (operators.empty()) {
                        operators.push(item);
                        isPushed = true;
                    }

                    // If the top of stack is closing parenthesis, push operator on stack
                    else if (operators.peek() == ')') {
                        operators.push(item);
                        isPushed = true;
                    }

                    // If it has same or higher priority than the top of stack, push operator on stack
                    else if (Tool.hasGraterPriorityThan(item, operators.peek())) {
                        operators.push(item);
                        isPushed = true;
                    }

                    // Else pop the operator from the stack and add it to prefix string
                    else {
                        prefixString.push(operators.pop());
                        prefixString.push(' ');
                    }
                }

                // And skip empty element
                infixString.pop();
            }

            // If it is a opening parenthesis, pop operators from stack and add them to prefix string
            // until a closing parenthesis is encountered. Pop and discard the closing parenthesis.
            else if (item == '(') {
                while (operators.peek() != ')') {
                    prefixString.push(operators.pop());
                    prefixString.push(' ');
                }
                operators.pop();

                // And skip empty element if parenthesis isn't last element of infixString
                if (!infixString.empty()) {
                    infixString.pop();
                }
            }

            // If it digit or symbol add it to prefix string
            else if (Tool.isDigit(item) || Tool.isSymbol(item)) {
                prefixString.push(item);

                if (!infixString.empty()) {
                    char nextItem = infixString.peek();
                    if (nextItem == ' ') {
                        prefixString.push(infixString.pop());
                    }
                } else {
                    prefixString.push(' ');
                }
            }
        }

        // If there is no more input, unstack the remaining operators and add them to output string.
        while (!operators.empty()) {
            prefixString.push(operators.pop());
            prefixString.push(' ');
        }

        // Return prefix string
        return stackIntoString(prefixString);
    }

    // Compute prefix notation
    public static double computePrefixNotation (String inputString) throws Exception {
        // Create stack of variables
        Stack<Double> numbers = new Stack<>();

        // Go through prefix notation
        for (int i = inputString.length() - 1; i >= 0; --i) {
            // Get ith character
            char item = inputString.charAt(i);

            // If character is delimiter then skip it
            if (item == ' ') {
                continue;
            }

            // Get all digits of a number and push it into numbers stack
             if (Tool.isDigit(item)) {
                // Create number variable
                double number = 0;

                // Create a pointer for computing number
                int j = i;

                // Set i into pointer of last digit of number
                char nextItem = item;
                while (Tool.isDigit(nextItem)) {
                    i = i - 1;
                    nextItem = inputString.charAt(i);
                }
                i = i + 1;

                // From [i, j] of input string compute a number
                for (int k = i; k <= j; ++k) {
                    char digit = inputString.charAt(k);
                    number = (number * 10.0) + (double) (digit - '0');
                }

                // Push number into numbers stack
                numbers.push(number);
            }

            // If character is operator
            else if (Tool.isOperator(item)) {
                 // Get two numbers from numbers stack
                 double firstNumber = numbers.pop();
                 double lastNumber = numbers.pop();

                 // Compute expression according to operator
                 double result = Tool.computeExpression(firstNumber, lastNumber, item);

                 // Push result of computing into numbers stack
                 numbers.push(result);
            }
        }

        // Return result of computation
        return numbers.pop();
    }
}
