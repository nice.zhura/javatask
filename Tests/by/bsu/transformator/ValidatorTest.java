package by.bsu.transformator;

import org.junit.jupiter.api.Test;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {
    // Check the work of validator of input string with different number of parenthesis
    @Test
    void testInputStringValidatorWithBadNumberOfParenthesis() {
        // Initialize string with different number of parenthesis
        String badString = " 10 * ( 3 + ( 4 / 4 ) ) )";

        Exception exception = assertThrows(Exception.class, () -> Validator.inputStringValidator(badString));

        String expectedMessage = "Different number of opening and closing parenthesis!";
        String actualMessage = exception.getMessage();

        // Check if method throws expected message
        assertTrue(actualMessage.contains(expectedMessage));
    }

    // Check the work of validator of input string with bad template
    @Test
    void testInputStringValidatorWithBadStringTemplate() throws Exception {
        // Get bad strings from excel table

        // Create path variable
        String path = "TestData/BadInputStrings.xls";

        // Get information from table according to the path
        Stack<String> table = Tool.getRowsFromTable(path);

        // Go through the stack
        while (!table.empty()) {
            // Get message from table
            String message = table.pop();

            // Get bad string from table
            String badStringTemplate = table.pop();

            // Print \n for error messages
            System.out.println();

            Exception exception = assertThrows(Exception.class, () -> Validator.inputStringValidator(badStringTemplate));

            String expectedMessage = "The string doesn't match the template!";
            String actualMessage = exception.getMessage();

            // Check if method throws expected message
            assertTrue(actualMessage.contains(expectedMessage), message);
        }
    }

    // Check the work of validator of good string
    @Test
    void testInputValidatorWithGoodInputString() {
        // Initialize string
        String goodInputString = "( 10 * ( 3 + ( 4 / 4 ) ) )";

        // Check if method doesn't throw any exception
        assertDoesNotThrow(() -> Validator.prefixStringValidator(goodInputString));
    }

    // Check the work of prefix string validator
    @Test
    void testPrefixStringValidator() {
        // Test bad string format

        // Initialize symbolic string
        String symbolicString = "( 10 * ( 3A + ( 4 / 4 ) ) )";

        Exception exception = assertThrows(Exception.class, () -> Validator.prefixStringValidator(symbolicString));

        String expectedMessage = "Input string in prefix notation isn't numeric expression!";
        String actualMessage = exception.getMessage();

        // Check if method throws expected message
        assertTrue(actualMessage.contains(expectedMessage));

        // Test good string format

        // Initialize numeric string
        String numericString = "( 10 * ( 3 + ( 4 / 4 ) ) )";

        // Check if method doesn't throw any exception if string is numeric
        assertDoesNotThrow(() -> Validator.prefixStringValidator(numericString));
    }
}