package by.bsu.transformator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ToolTest {
    // Check if method works smoothly
    @Test
    void testHasGraterPriorityThan() throws Exception {
        // Initialize operators
        char firstOperator = '*';
        char secondOperator = '+';

        // Test method
        assertTrue(Tool.hasGraterPriorityThan(firstOperator, secondOperator));

        // Test method changing operator places
        assertFalse(Tool.hasGraterPriorityThan(secondOperator, firstOperator));

        // Initialize bad operator
        char badOperator = '^';

        Exception exception = assertThrows(Exception.class, () -> Tool.hasGraterPriorityThan(firstOperator, badOperator));

        String expectedMessage = "Wrong operator!";
        String actualMessage = exception.getMessage();

        // Check that method throws exception if we have wrong operator
        assertTrue(actualMessage.contains(expectedMessage));

    }

    // Check that method throws exception when we have bad operator
    @Test
    void testComputeExpression() {
        // Initialize bad operator
        char badOperator = '^';

        // Initialize numbers
        double firstNumber = 2.0;
        double lastNumber = 5.0;

        Exception exception = assertThrows(Exception.class, () -> Tool.computeExpression(firstNumber, lastNumber, badOperator));

        String expectedMessage = "Wrong operator in computing expression";
        String actualMessage = exception.getMessage();

        // Check that method throws exception if we have wrong operator
        assertTrue(actualMessage.contains(expectedMessage));
    }
}