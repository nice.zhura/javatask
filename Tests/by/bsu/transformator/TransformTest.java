package by.bsu.transformator;

import org.junit.jupiter.api.Test;
import java.util.Stack;

import static org.junit.jupiter.api.Assertions.*;

class TransformTest {
    // Check transformation of non-empty string in infix notation into prefix notation
    @Test
    void testInfixToPrefix() throws Exception {
        // Create path variable
        String path = "TestData/InfixToPrefixData.xls";

        // Get information from table according to the path
        Stack<String> table = Tool.getRowsFromTable(path);

        // Go through the stack
        while (!table.empty()) {
            // Get initial data
            String prefixString = table.pop();
            String infixString = table.pop();

            // Create message
            String message = "Error in test infix string: " + infixString;
            System.out.println();

            // Compare with method result
            assertEquals(prefixString, Transform.infixToPrefix(infixString), message);
        }
    }

    // Check infix to prefix transformation with empty string
    @Test
    void testInfixToPrefixWithEmptyString() {
        // Initialize symbolic string
        String emptyString = "";

        Exception exception = assertThrows(Exception.class, () -> Transform.infixToPrefix(emptyString));

        String expectedMessage = "Input string is empty!";
        String actualMessage = exception.getMessage();

        // Check if method throws expected message
        assertTrue(actualMessage.contains(expectedMessage));
    }

    // Check computing prefix notation of non-empty strings
    @Test
    void testComputePrefixNotation() throws Exception {
        // Create path variable
        String path = "TestData/PrefixToComputeData.xls";

        // Get information from table according to the path
        Stack<String> table = Tool.getRowsFromTable(path);

        // Go through the stack
        while (!table.empty()) {
            // Get initial data
            double result = Double.parseDouble(table.pop());
            String prefixString = table.pop();

            // Create message
            String message = "Error in test prefix string: " + prefixString;
            System.out.println();

            // Compare with method result
            assertEquals(result, Transform.computePrefixNotation(prefixString) , message);
        }
    }

    // Check division by zero in computation prefix notation
    @Test
    void testDivisionByZeroInComputePrefixNotation() {
        // Initialize string with division by zero
        String prefixString = " * 10 + 3 / 4 0";

        Exception exception = assertThrows(Exception.class, () -> Transform.computePrefixNotation(prefixString));

        String expectedMessage = "Division by zero in computing expression!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

}